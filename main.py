"""
Retrieves the list of locations from UoN CampusM and converts them to KML.
"""

import requests

import xml.etree.ElementTree as ET

URL = 'https://www.ombielservices.co.uk/axis2v3/services/CampusMService/cachedMapPosition'
NS = {'ns1': 'http://campusm.gw.com/campusm'}

def generate_request_body(org_code, person_id, password, profile_id):
    body = ET.Element('cachedMapPositionRequest')
    body.attrib['xmlns'] = 'http://campusm.gw.com/campusm'
    ET.SubElement(body, 'orgCode').text = org_code
    ET.SubElement(body, 'personId').text = person_id
    ET.SubElement(body, 'password').text = password
    ET.SubElement(body, 'profileId').text = profile_id
    return body

def main():
    """
    Credentials can be found by proxying the UoN app and pulling them out
    from the request body of requests to the above url.
    """
    body = generate_request_body(
        '973',
        '2297393',
        'IkVBKeMBVmp/vrspjyLujvj5tF/UKJlp/k/vF4sYkH8=',
        '2902')
    req = requests.post(
        URL,
        ET.tostring(body, 'utf8'),
        headers={'Content-Type': 'application/xml'})
    xml_from = ET.fromstring(req.text)
    xml_to = ET.Element('kml')
    xml_to.attrib['xmlns'] = 'http://www.opengis.net/kml/2.2'
    document = ET.SubElement(xml_to, 'Document')
    ET.SubElement(document, 'name').text = 'CampusM Import'
    for item in xml_from.find('ns1:cachedMapItems', NS):
        for litem in item.find('ns1:locations', NS):
            for pitem in litem.find('ns1:positions', NS):
                p = ET.SubElement(document, 'Placemark')
                ET.SubElement(p, 'name').text = pitem.find('ns1:desc', NS).text
                desc = ''
                note = pitem.find('ns1:note', NS)
                if note != None:
                    desc = '{}<p>{}</p>'.format(desc, note.text)
                img = pitem.find('ns1:img', NS)
                cat = pitem.find('ns1:locCatDesc', NS)
                if cat != None:
                    desc = '{}<p class="category">{}</p>'.format(desc, cat.text)
                if img != None:
                    desc = '{}<img class="image" src="{}" />'.format(desc, img.text)
                ET.SubElement(p, 'description').text = desc
                point = ET.SubElement(p, 'Point')
                ET.SubElement(point, 'coordinates').text = '{},{},0'.format(
                    pitem.find('ns1:longitude', NS).text,
                    pitem.find('ns1:latitude', NS).text)

    print(ET.tostring(xml_to, encoding='UTF-8').decode('utf8'))

if __name__ == '__main__':
    main()
